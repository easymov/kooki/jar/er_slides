# er_slides

This kooki jar is a presentation template.

## Prerequisites

Make sure the Easymov document toolchain is installed and configured.
You can look at the tutorial at [gitlab.com/easymov/kooki/document_toolchain](https://gitlab.com/easymov/kooki/document_toolchain).

## Quickstart

```bash
kooki install er_slides
gener8 er_slides
# choose in the prompt the name of your folder
cd <folder_name>
kooki bake --update
```

Edit the *extensions* and the *metadata.yaml* to match your needs.

## Extensions

*~content.md*
: Markdown file containing the abstract

## Metadata

```yaml
title: XXXX
```

## Content

Slides are separated by the characters `---`.
For examples.

```

# slide 1

---

# slide 2

---

# slide 3

```